package com.example.felipeconvo.flashlightexample;

import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private static String TAG = MainActivity.class.getSimpleName();
    private static String ERR_CAMERA_404 = "CAMERA IS NULL";
    private static String ERR_UNKNOWN_520 = "UNKOWN ERROR";

    private Camera camera;
    private Handler handler;
    private boolean isFlashing;
    private boolean wasStopTriggered;
    private Runnable flashToggler = new Runnable() {
        @Override
        public void run() {
            Log.d(TAG, " **** TOGGLING FLASH **** ");

            if (wasStopTriggered) {
                return;
            }

            if (handler == null) {
                handler = new Handler();
            }

            if (camera == null) {
                return;
            }

            String currentFlashMode = camera.getParameters().getFlashMode();
            Camera.Parameters parameters = camera.getParameters();

            if (Camera.Parameters.FLASH_MODE_OFF.equals(currentFlashMode)) {
                parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                camera.setParameters(parameters);
            } else if (Camera.Parameters.FLASH_MODE_TORCH.equals(currentFlashMode)) {
                parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                camera.setParameters(parameters);
            } else {
                Log.w(TAG, ERR_UNKNOWN_520);
            }

            if (!wasStopTriggered) {
                handler.postDelayed(flashToggler, 1000);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.turnOn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startFlashing();
            }
        });

        findViewById(R.id.turnOff).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopFlashing();
            }
        });

        initCamera();
    }

    @Override
    protected void onResume() {
        super.onResume();

        startFlashing();
    }

    @Override
    protected void onStop() {
        super.onStop();

        stopFlashing();
    }

    @Override
    public void finish() {
        super.finish();

        stopFlashing();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        stopFlashing();
    }

    private void initCamera() {
        try {
            releaseCamera();
        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }

        try {
            camera = Camera.open();

            if (null != camera) {
                Camera.Parameters params = camera.getParameters();

                params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                camera.setParameters(params);
            } else {
                Log.w(TAG, ERR_CAMERA_404);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void startFlashing() {
        Log.d(TAG, " **** CALLING FLASH **** ");

        if (isFlashing) {
            return;
        }

        if (camera == null) {
            initCamera();
        }

        if (camera == null) {
            return;
        }

        SurfaceTexture dummy = new SurfaceTexture(1);

        try {
            camera.setPreviewTexture(dummy);
        } catch (IOException e) {
            e.printStackTrace();
        }

        camera.startPreview();
        flashToggler.run();

        isFlashing = true;
    }

    public void stopFlashing() {
        Log.d(TAG, " **** CALLING STOP FLASH **** ");

        wasStopTriggered = true;

        if (handler != null) {
            handler.removeCallbacks(flashToggler);
            handler = null;
        }

        releaseCamera();

        wasStopTriggered = false;
        isFlashing = false;
    }

    private void releaseCamera() {
        if (null != camera) {
            camera.stopPreview();
            camera.release();

            camera = null;
        }
    }

}
